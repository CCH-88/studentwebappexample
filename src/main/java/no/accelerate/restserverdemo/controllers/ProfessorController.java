package no.accelerate.restserverdemo.controllers;

import no.accelerate.restserverdemo.models.Professor;
import no.accelerate.restserverdemo.services.professor.ProfessorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/professors")
public class ProfessorController {

    private final ProfessorService professorService;

    public ProfessorController(ProfessorService professorService) {
        this.professorService = professorService;
    }

    @GetMapping
    public ResponseEntity<Collection<Professor>> getAll() {
        return ResponseEntity.ok(professorService.findAll());
    }

    @PostMapping
    public ResponseEntity add(@RequestBody Professor professor) {
        professorService.add(professor);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
