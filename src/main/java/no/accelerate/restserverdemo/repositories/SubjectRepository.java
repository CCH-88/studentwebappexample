package no.accelerate.restserverdemo.repositories;

import no.accelerate.restserverdemo.models.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository (DAO) for the Professor domain class.
 * Uses @Query for business logic that is difficult to achieve with default functionality.
 */
@Repository
public interface SubjectRepository extends JpaRepository<Subject, Integer> {
}
