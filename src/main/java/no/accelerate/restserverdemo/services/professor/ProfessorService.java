package no.accelerate.restserverdemo.services.professor;


import no.accelerate.restserverdemo.models.Professor;
import no.accelerate.restserverdemo.services.CrudService;

/**
 * Service for the Professor domain class.
 * Providing basic CRUD functionality through CrudService and any extended functionality.
 */
public interface ProfessorService extends CrudService<Professor, Integer> {
}
