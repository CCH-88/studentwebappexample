package no.accelerate.restserverdemo.services.student;


import no.accelerate.restserverdemo.models.Student;
import no.accelerate.restserverdemo.services.CrudService;

import java.util.Collection;

/**
 * Service for the Student domain class.
 * Providing basic CRUD functionality through CrudService and any extended functionality.
 */
public interface StudentService extends CrudService<Student, Integer> {
    Collection<Student> findAllByName(String name);
}
