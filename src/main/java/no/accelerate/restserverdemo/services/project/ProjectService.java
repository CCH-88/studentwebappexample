package no.accelerate.restserverdemo.services.project;

import no.accelerate.restserverdemo.models.Project;
import no.accelerate.restserverdemo.services.CrudService;

public interface ProjectService extends CrudService<Project, Integer> {
}
