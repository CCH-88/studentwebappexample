package no.accelerate.restserverdemo.mappers;

import no.accelerate.restserverdemo.models.Project;
import no.accelerate.restserverdemo.models.dtos.project.ProjectDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ProjectMapper {
    ProjectDTO projectToProjectDto(Project project);
    // Mapping the other way
    @Mapping(target = "student", ignore = true)
    Project projectDtoToProject(ProjectDTO projectDTO);
}
